package com.example.myfirstapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>  
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // 测试联动状态流转  提交流转转态 相同规则流转
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.myfirstapp", appContext.getPackageName());
    }
}
